#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass elsart
\begin_preamble
 \newcommand{\field}[1]{\mathbb{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Space to make more readable!
%\vspace{10 mm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Take out later!
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 0
\use_package esint 1
\use_package mathdots 0
\use_package mathtools 0
\use_package mhchem 0
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
pagestyle
\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

{
\end_layout

\end_inset

empty
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\align center

\size larger
CS2233 Discrete Mathematical Structures Spring 2018 
\begin_inset Newline newline
\end_inset

 
\series bold
\size large
Homework 3
\series default

\begin_inset Newline newline
\end_inset

 
\size normal
Due 2/9/17 before 11:59pm 
\begin_inset Newline newline
\end_inset

 
\series bold
Bruce Orcutt 
\end_layout

\begin_layout Standard

\series bold
\size normal
Annotate all your proofs with comments/text in order to receive full credit.
\end_layout

\begin_layout Standard

\series bold
\size normal
1.
 Contrapositive and contradiction (4 points)
\end_layout

\begin_layout Standard

\series bold
\size normal
Consider the following claim:
\begin_inset Newline newline
\end_inset

 For all integers 
\begin_inset Formula $m$
\end_inset

 and 
\begin_inset Formula $n$
\end_inset

, if 
\begin_inset Formula $m-n$
\end_inset

 is odd then 
\begin_inset Formula $m$
\end_inset

 is odd or 
\begin_inset Formula $n$
\end_inset

 is odd.
 
\end_layout

\begin_layout Enumerate

\series bold
\size normal
(2 points) Prove the claim using a proof by contrapositive 
\begin_inset Newline newline
\end_inset

 ANSWER : 
\begin_inset Tabular
<lyxtabular version="3" rows="8" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Step
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Justification
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\size normal
For all integers 
\begin_inset Formula $m$
\end_inset

 and 
\begin_inset Formula $n$
\end_inset

, if 
\begin_inset Formula $m$
\end_inset

 is even and 
\begin_inset Formula $n$
\end_inset

 is even, then 
\begin_inset Formula $m-n$
\end_inset

 is even
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Definition of Contrapositive
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
\begin_inset Formula $m=2a$
\end_inset

 ;
\begin_inset Formula $n=2b$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Definition of Even
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\size normal
\begin_inset Formula $2a-2b=2(a-b)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Distributive Law
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
\begin_inset Formula $m-n=2(a-b)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
substitution of values
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\size normal
\begin_inset Formula $m-n$
\end_inset

 is even
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Definition of Even
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\size normal
 Because 
\begin_inset Formula $m-n$
\end_inset

 is even, and 
\begin_inset Formula $m$
\end_inset

 and 
\begin_inset Formula $n$
\end_inset

 are even, the contrapositive is true
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\size normal
Because the contrapositive is true, the original statement is true.
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\size normal
 
\series default
\size default
Definition of Contrapositive
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Enumerate

\series bold
\size normal
For all integers 
\begin_inset Formula $m$
\end_inset

 and 
\begin_inset Formula $n$
\end_inset

, if 
\begin_inset Formula $m$
\end_inset

 is even and 
\begin_inset Formula $n$
\end_inset

 is even, then 
\begin_inset Formula $m-n$
\end_inset

 is even (Definition of Contrapositive)
\begin_inset Newline newline
\end_inset

 
\begin_inset Formula $m=2a:n=2b$
\end_inset

 (Definition of Even) 
\begin_inset Newline newline
\end_inset

 
\begin_inset Formula $2a-2b=2(a-b)$
\end_inset

 
\begin_inset space \quad{}
\end_inset

(Distributive Law) 
\begin_inset Newline newline
\end_inset

 
\begin_inset Formula $m-n=2(a-b)$
\end_inset

 
\begin_inset space \quad{}
\end_inset

(Substitution of Values)
\begin_inset Newline newline
\end_inset

 
\begin_inset Formula $m-n$
\end_inset

 is even 
\begin_inset space \quad{}
\end_inset

(Definition of Even)
\begin_inset Newline newline
\end_inset

 Because 
\begin_inset Formula $m-n$
\end_inset

 is even, and 
\begin_inset Formula $m$
\end_inset

 and 
\begin_inset Formula $n$
\end_inset

 are even, the contrapositive is true.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Enumerate

\series bold
\size normal
(2 points) Prove the claim using a proof by contradiction 
\end_layout

\begin_layout Standard

\series bold
\size normal
2.
 Equivalence (4 points)
\end_layout

\begin_layout Standard

\series bold
\size normal
Prove the following for all 
\begin_inset Formula $x\in\field{R}$
\end_inset

:
\begin_inset Newline newline
\end_inset

 
\begin_inset Formula $x$
\end_inset

 is rational 
\begin_inset Formula $\Leftrightarrow x-5$
\end_inset

 is rational 
\begin_inset Formula $\Leftrightarrow x/3$
\end_inset

 is rational.
\end_layout

\begin_layout Standard

\series bold
\size normal
3.
 Proof by cases (3 points)
\end_layout

\begin_layout Standard

\series bold
\size normal
Use a proof by cases to show that: 
\begin_inset Formula 
\[
(\max(x,y)+\min(x,y))^{2}+\min(x,y)\max(x,y)=x^{2}+3xy+y^{2}
\]

\end_inset

where 
\begin_inset Formula $x,y,z\in\field{R}$
\end_inset

.
\end_layout

\begin_layout Standard

\series bold
\size normal
4.
 Rational, irrational (4 points)
\end_layout

\begin_layout Enumerate

\series bold
\size normal
(2 points) Prove or disprove that if 
\begin_inset Formula $x^{y}$
\end_inset

 is an irrational number, then 
\begin_inset Formula $x$
\end_inset

 and 
\begin_inset Formula $y$
\end_inset

 are also irrational numbers.
 
\end_layout

\begin_layout Enumerate

\series bold
\size normal
(2 points) Prove that if 
\begin_inset Formula $x^{2}$
\end_inset

 is irrational, then 
\begin_inset Formula $x$
\end_inset

 is irrational.
 (HINT: try a proof by contrapositive) 
\end_layout

\begin_layout Standard

\series bold
\size normal
5.
 Sets (6 points)
\end_layout

\begin_layout Enumerate

\series bold
\size normal
(1.5 points) Use set builder notation to give a description of the set 
\begin_inset Formula $\{-3,-2,-1,0,1,2,3,4,5\}$
\end_inset

.
 
\end_layout

\begin_layout Enumerate

\series bold
\size normal
(2 points) Let 
\begin_inset Formula $A=\{a,b,c\}$
\end_inset

, 
\begin_inset Formula $B=\{x,y\}$
\end_inset

, and 
\begin_inset Formula $C=\{5,10\}$
\end_inset

.
 Find 
\begin_inset Formula $A\times B\times C$
\end_inset

 and 
\begin_inset Formula $C\times A\times B$
\end_inset

.
 
\end_layout

\begin_layout Enumerate

\series bold
\size normal
(2.5 points) Let 
\begin_inset Formula $A=\{1,4,8,16\}$
\end_inset

 and 
\begin_inset Formula $B=\{2,4,16,32,64\}$
\end_inset

.
 Find 
\begin_inset Formula $A\cup B$
\end_inset

, 
\begin_inset Formula $A\cap B$
\end_inset

, 
\begin_inset Formula $A\backslash B$
\end_inset

, 
\begin_inset Formula $B\backslash A$
\end_inset

, and 
\begin_inset Formula $|\mathcal{P}(A)|$
\end_inset

.
 
\end_layout

\begin_layout Standard

\series bold
\size normal
6.
 Set Theorems (3 points)
\end_layout

\begin_layout Standard

\series bold
\size normal
Prove 
\begin_inset Formula $A\cup(A\cap B)=A$
\end_inset

.
\end_layout

\end_body
\end_document
